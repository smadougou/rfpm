#!/usr/bin/python
import subprocess
import re
import sys

rh = open('gtx480call.csv', 'w')
hwritten = 0
header = ['size','time']

metrics = ['branch_efficiency','warp_execution_efficiency','issue_slot_utilization','l2_read_transactions','l2_write_transactions','l2_read_throughput','l2_write_throughput','flops_sp','flops_dp','flops_sp_special','inst_replay_overhead','alu_fu_utilization','ldst_fu_utilization','inst_issued','inst_executed','achieved_occupancy','gld_requested_throughput','gst_requested_throughput','gld_throughput','gst_throughput','gld_request','gst_request','l1_global_load_miss','l1_global_load_hit','global_store_transaction','uncached_global_load_transaction','shared_load','shared_store','l1_local_load_miss','l1_local_load_hit','local_store','l1_shared_bank_conflict','divergent_branch','branch']

def unit(ts):
	if re.match(r'\d+\.\d+us', ts): 
		return "us"
        if re.match(r'\d+\.\d+ms', ts):
                return "ms"
        if re.match(r'\d+\.\d+s', ts):
                return "s"

calls1 = 1
calls2 = 1
contrib1 = 1
contrib2 = 1
for i in range(64,8193,64):
	metricval = {}
	metricval['time'] = sys.float_info.max
        for metric in metrics:
               	metricval[ metric ] = sys.float_info.max
        cmetrics = {}
        for metric in metrics:
                cmetrics[ metric ] = 0
	jep = "nw" + str(i) + ".e"
	ecmd = "find . -name " + jep + "*"
	finde = subprocess.Popen(ecmd, stdout=subprocess.PIPE, shell=True)
	(jef, err) = finde.communicate()
	vline = []
	
	vline.append(str(i))

	eh = open(jef.strip())
	isTime = 0
       	for line in eh:
		if "Time" in line:
			isTime = 1
		elif isTime:
			if "needle_cuda_shared_1" in line:
				tokens = re.split('\s+', line.strip())
				contrib1 = float(tokens[0][:-1])
				calls1 = int(tokens[2])
				v = tokens[3]
				if "s" == unit(v):
					metricval['time'] = contrib1 * 1000 * float(v[:-1])
				elif "us" == unit(v):
					metricval['time'] = contrib1 * (float(v[:-2])/1000)
				else:
					metricval['time'] = contrib1 * float(v[:-2])
                        elif "needle_cuda_shared_2" in line:
                                isTime = 0
                                tokens = re.split('\s+', line.strip())
				contrib2 = float(tokens[0][:-1])
				calls2 = int(tokens[2])
				v = tokens[3]
                                if "s" == unit(v):
					val = (metricval['time'] + contrib2 * 1000 * float(v[:-1])) / (contrib1 + contrib2)
                                        vline.append(str(val))
                                elif "us" == unit(v):
                                        val = (metricval['time'] + contrib2 * float(v[:-2])/1000) / (contrib1 + contrib2)
                                        vline.append(str(val))					
                                else:
                                        val = (metricval['time'] + contrib2 * float(v[:-2])) / (contrib1 + contrib2)
                                        vline.append(str(val))
		else:
			for metric in metrics:
				if (re.match(r'.*\s+'+metric+'\s+.*', line.strip())):
					tokens = re.split('\s+', line.strip())
					v = tokens[len(tokens)-1]
					if "throughput" in metric:
						if cmetrics[metric] == 0: 
							if "MB/s" in v:
								metricval[metric] = contrib1 * (float(v[:-4])/1000)
							else:
								metricval[metric] = contrib1 * float(v[:-4])
                                                elif cmetrics[metric] == 1: 
                                                        if "MB/s" in v:
								vline.append(str((metricval[metric] + contrib2 * float(v[:-4])/1000) / (contrib1 + contrib2)))
                                                        else:
								vline.append(str((metricval[metric] + contrib2 * float(v[:-4])) / (contrib1 + contrib2)))
					else:
                                               	if "utilization" in metric or "efficiency" in metric:
							if cmetrics[metric] == 0:
								metricval[metric] = contrib1 * float(v[1:-1])
                                                        elif cmetrics[metric] == 1:
                                                                vline.append(str((metricval[metric] + contrib2 * float(v[1:-1])) / (contrib1 + contrib2)))
                                               	elif metric != "achieved_occupancy" and metric != "ipc" and "overhead" not in metric:
                                               		if cmetrics[metric] == 0:
								metricval[metric] = contrib1 * float(v)/(calls1*float(vline[1]))
                                                        elif cmetrics[metric] == 1:
								vline.append(str((metricval[metric] + contrib2 * float(v)/(calls2*float(vline[1]))) / (contrib1 + contrib2)))
                                               	else:
							if cmetrics[metric] == 0:
								metricval[metric] = contrib1 * float(v)
                                                        elif cmetrics[metric] == 1:
                                                                vline.append(str((metricval[metric] + contrib2 * float(v)) / (contrib1 + contrib2)))
					cmetrics[metric] += 1;
					if metric not in header:
						header.append(metric)
	eh.close()
	if (hwritten == 0):
		print "hwritten=", hwritten, "\n"
		sheader = ','.join(header) + '\n'
		rh.writelines(sheader)
		hwritten = 1
       	vline = ','.join(vline) + '\n'
       	rh.writelines(vline)

rh.close()
