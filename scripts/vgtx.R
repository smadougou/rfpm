pdf('valgtx.pdf')
library(randomForest)
library(ROCR)
gtx <- read.csv('gtx580call.csv')
sample = rbinom(dim(gtx) [1], 1, .2)
trainset = gtx[sample==0,]
evalset = gtx[sample==1,]
set.seed(123)
trainset <- trainset[-1]
bmt <- tuneRF(trainset[,-1], trainset$time, ntreeTry=500, stepFactor=2, improve=0.01, trace=TRUE, dobest=TRUE, plot=F)
bmt
rf.all <-randomForest(time ~ ., data=trainset, mtry=16, nodesize=3, ntree=1000, keep.forest=TRUE, importance=TRUE)#,test=evalset)
rf.6 <-randomForest(time ~ global_store_transaction+gst_throughput+gst_request+gst_requested_throughput+achieved_occupancy+inst_issued, data=trainset, mtry=6, nodesize=3, ntree=1000, keep.forest=TRUE, importance=TRUE)#,test=evalset)

rf
preds.all <- predict(rf.all, newdata=evalset)
preds.6 <- predict(rf.6, newdata=evalset)

plot(evalset$size,preds.all, type="l", lty=2, xlab="size",ylab="time (ms)")
lines(evalset$size,preds.6, col="blue", lty=1)
lines(evalset$size,evalset$time, lty=3, col="red")
legend("top", legend=c("predicted all","predicted 6","measured"), lty=c(2,1,3), col=c("black","blue","red"),bty="n")

