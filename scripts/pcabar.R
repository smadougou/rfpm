foo <- function(dat) {
    out <- lapply(dat, function(x) length(unique(x)))
    want <- which(!out > 1)
    unlist(want)
}

zeroVar <- function(data, useNA = 'ifany') {
    out <- apply(data, 2, function(x) {length(table(x, useNA = useNA))})
    which(out==1)
}

pdf('pca_gtx_small.pdf')
data <- read.csv('gtx480call.csv')
data <- data[-1]
data <- data[-1]
#ivs <- c("size","l1_global_load_miss","inst_executed","branch","l2_read_transactions","inst_replay_overhead","gld_request","inst_issued") #call2
#ivs <- c("l2_read_transactions","shared_store","global_store_transaction","shared_load","gst_request","gld_request","l2_write_transactions","l1_global_load_miss","inst_executed", "size") #raw2
#ivs <- c("inst_replay_overhead","shared_replay_overhead","l2_read_throughput","gst_requested_throughput","gld_throughput","gld_requested_throughput")
ivs <- c("l1_global_load_hit", "global_store_transaction", "ldst_fu_utilization", "l2_write_transactions", "inst_replay_overhead", "warp_execution_efficiency", "issue_slot_utilization")
data <- data[ivs]

#data <- data[, -zeroVar(data)] # when using all data need to normalize
fit <- prcomp(data,scale=T,tol=.5)

fit <- varimax(fit$rotation) # for varimax rotation

# Following 3 lines for barplot
loadtab <- t(fit$loadings)
par(las=2)
barplot(loadtab,beside=T,names.arg=colnames(loadings),col=c("black","white"),cex.names=.5)

# for scree plot
#screeplot(fit,main="Scree plot",xlab="components")

# for variable loading plot
#load = fit$loadings #fit$rotation
#sorted.loadings = load[order(load[,2]),1]
#dotchart(sorted.loadings,main="Loadings for PC1",xlab="Variable loadings")
