pdf('vik20_nosz.pdf')
library(randomForest)
gtx <- read.csv('k20call.csv')
gtx <- gtx[-1]
gtx
sample = rbinom(dim(gtx) [1], 1, 1)
trainset = gtx[sample==0,]
evalset = gtx[sample==1,]
set.seed(123)
bmt <- tuneRF(gtx[,-1], gtx$time, ntreeTry=1000, stepFactor=2, improve=0.01, trace=TRUE, dobest=TRUE, plot=F)
bmt
rf <-randomForest(time ~ ., data=evalset, mtry=16, nodesize=3, ntree=1000, keep.forest=TRUE, importance=TRUE, proximity=T)
rf
#partialPlot(rf, evalset, achieved_occupancy)
varImpPlot(rf,type=1,main=NULL)
